const { readFileSync } = require('fs');
const { isFalsy, isTruthy } = require('./utils');
const path = require('path');

const defaultOptions = {
  logFixedBuildings: false,
  logFixedDates: true,
}

const load = (filepath) => {
  return JSON.parse(readFileSync(path.resolve(filepath)).toString());
}

/**
 * Initial export of buildings
 * datacenters should have an array of buildings
 * @type {any}
 */
const buildings = load('./src/data/buildings.json');

/**
 * Initial export of datacenters
 * note: some records do not have correct date formats, or buildings associated
 * @type [{id: string, externalId: string, address: string, country: string, city: string, state: string, zipcode: string, owner: string, contactName: string, contactEmail: string, buildingIds: array, buildings: array, import: string, operationalDate: string}]
 */
const dataCenters = load('./src/data/datacenters.json');

/**
 * Provides an interface to datacenter and building data.
 * Also a reasonable place for data mutations/transforms. See `enableImport`;
 */
class DataClient {
  constructor(obj, options = defaultOptions) {
    const {
      buildings,
      dataCenters,
    } = obj;

    this.buildings = buildings;
    this.dataCenters = dataCenters;
    this.options = options;

    this.fixInvalidDates();
    this.fixInvalidBuildings();
  }

  /**
   * Datacenters without an id, and therefore insertable
   * @returns {*}
   */
  insertable() {
    return this.dataCenters.filter(({ id }) => isFalsy(id));
  }

  /**
   * Datacenters with an id, and therefore insertable
   * @returns {*}
   */
  updatable() {
    return this.dataCenters.filter(({ id }) => isTruthy(id));
  }

  /**
   * Ideally, we would pick out specific datacenters to delete. For testing purposes, Ive duplicated the updatable function
   * @returns {*}
   */
  deletable() {
    return this.dataCenters.filter(({ id }) => isTruthy(id));
  }

  /**
   * Exported records came with an import field set to false. To process
   * on the api this value must be true.
   */
  enableImport() {
    this.dataCenters = this.dataCenters.map((dc) => ({ ...dc, import: true }));
  }

  fixInvalidDates() {
    for( let i = 0; i < this.dataCenters.length; i++ ){
      var date = this.dataCenters[i].operationalDate;
      var ISO_date = new Date(date).toISOString();

      // Go to next entry if we have a valid date
      if(date === ISO_date) 
        continue;

      // Update the dataCenter record to the correct format
      this.dataCenters[i].operationalDate = ISO_date;

      // If the options flag is set, log the changed value
      if(this.options.logFixedDates){
        console.log("Converted operationalDate of value " + date + " to " + ISO_date);
      }
    }
  }

  fixInvalidBuildings() {
    for( let i = 0; i < this.dataCenters.length; i++ ){
      var obj = this.dataCenters[i];
      if(typeof obj.buildings !== 'undefined') continue;
      
      var buildingIds = obj.buildingIds;

      var repopulated_buildings = [];
      var repopulated_buildingIds = [];

    
      buildingIds.forEach( buildingId => {
        var building = this.findBuildingFromId(buildingId);
        if(building === null){
          console.log("Unable to find building of ID:", buildingId,"- pruning ID from dataset");
        }
        else {
          if(this.options.logFixedBuildings)
            console.log("Successfully populated building using ID:", buildingId);
          repopulated_buildings.push(building);
          repopulated_buildingIds.push(buildingId);
        }
      });

      this.dataCenters[i].buildings = repopulated_buildings;
      this.dataCenters[i].buildingIds = repopulated_buildingIds;
    }
  }

  findBuildingFromId(id) {
    this.buildings.forEach(building => {
      if(building.id === id){
        return building;
      }
    });

    return null;
  }
}

const dataClient = new DataClient({ buildings, dataCenters });

module.exports = { dataClient };
