Files
 - status.js -> ensures api-client.js works properly
 - api-client.js -> makes requests to an external URL -> dependent on utils.js
    - Contains a class ApiClient, which has the functions: endpoint, create, update, & status
 - utils.js -> full of helper functions utilized by other scripts
    - exports: template, isNullish, isFalsy, isTruthy, combineCounts, simplifyResults, isJson, createErrorSummary, logErrorSummary, createValidationSummary, logValidationSummary, colors
 - runner.js -> calls the Import.start() function from index.js, wrapped with error handling
 - index.js -> (UNSURE)
    - dependencies: apiClient, dataClient, Batch, Utils
 - data-client.js -> Stores and handles buildings/datacenters. Tells you whether pieces of data are insertable/updatable.
    - dependencies: buildings.json, datacenters.json
 - batch.js -> handles processing a series of requests.
    - dependencies: utils, dataclient, apiclient, request
 - request.js -> handles any given isolated API call. Contains options which we can set to change logging/error reporting

After changing the "logResponse" flag to true and the "suppressErrors" flag to false in request.js, I was able to identify the following errors:
 - Error 422: Unprocessable entity
    - This seemed to have the most tackleable problems, with messages like:
        'operationalDate (972107480155) must be a ISO 8601 compliant string.'
        'operationalDate (426356775923) must be a ISO 8601 compliant string.'
        'operationalDate (1165316996550) must be a ISO 8601 compliant string.',
        'buildings property must be provided as an array of building objects'
 - Error 429: Rate limit exceeded. 10 requests per 5000ms
    - This seemed like a more difficult problem to solve.
    - Perhaps I needed to find a different API call which minimized the number of requests necessary
 - Error 503: Service Unavailable
    - We can't really make the API 100% reliable. But if a request is denied we can retry until it succeeds

Error 422 has to do with the data in datacenters.json
 - Some dates are formatted like this: "2000-02-27T11:09:08.968Z", which is good
 - Others are formatted like this: "1140145484198", which is bad. 
    - This is also not in UNIX timestamp format, because it mapped to the year 38897
 - My second guess was that the wrong format would map to the correct format, if we converted decimal to binary to string
    - This didn't seem to line up either. I was left confused on how I ought to interpret and correct the wrong dates

The [Redacted] API documentation had some information, but wasn't helpful with the odd datetimes

Upon further digging, the datetime format did seem to be in the form of UNIX timestamps
 - I thought it was wrong because I initially converted it as if the number represented seconds.
 - The number actually represented milliseconds.
 - I was able to properly convert all of the dates and fix half of the error 422 codes.

The other half of the error codes posed an issue:
 - They occured because many datacenter entries did not contain a valid "buildings" list
 - These entries did have a valid "buildingIds" list
 - I tried cross-referencing with the buildings.json file, but was unable to find matching buildings.